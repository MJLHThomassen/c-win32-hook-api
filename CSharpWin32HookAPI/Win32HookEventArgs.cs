﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 01-01-2015
 * 
 */

using System;

namespace CSharpWin32HookAPI
{
    public class Win32HookEventArgs<TwParam, TlParam> : EventArgs
    {
        public int NCode { get; private set; }
        public TwParam WParam { get; private set; }
        public TlParam LParam { get; private set; }
        public int? ReturnValue { get; set; }

        public Win32HookEventArgs(int nCode, TwParam wParam, TlParam lParam)
        {
            NCode = nCode;
            WParam = wParam;
            LParam = lParam;
            ReturnValue = null;
        }
    }

    public class Win32HookEventArgs<TnCode, TwParam, TlParam> : EventArgs
    {
        public TnCode NCode { get; private set; }
        public TwParam WParam { get; private set; }
        public TlParam LParam { get; private set; }
        public int? ReturnValue { get; set; }

        public Win32HookEventArgs(TnCode nCode, TwParam wParam, TlParam lParam)
        {
            NCode = nCode;
            WParam = wParam;
            LParam = lParam;
            ReturnValue = null;
        }
    }
}
