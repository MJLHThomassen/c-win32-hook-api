﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 03-01-2015
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSharpWin32HookAPI.Extensions;

namespace CSharpWin32HookAPI
{
    public static class Win32Hooks
    {
        #region DLL Imports
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, HOOKPROC lpfn, IntPtr hMod, int dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        // TODO: Figure out how to implement pInputs in a managed struct
        //[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //private static extern uint SendInput(uint nInputs, IntPtr pInputs, int cbSize);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetCurrentThreadId();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetLastError();
        #endregion

        #region Delegates & Event Defenitions
        private delegate IntPtr HOOKPROC(int nCode, IntPtr wParam, IntPtr lParam);

        private static EventHandler<Win32HookEventArgs<CurrentThreadIndicator, CWPSTRUCT>> _callWndHookEvent;
        private static EventHandler<Win32HookEventArgs<CurrentThreadIndicator, CWPRETSTRUCT>> _callWndRetHookEvent;
        private static EventHandler<Win32HookEventArgs<object, object>> _cbtHookEvent;
        private static EventHandler<Win32HookEventArgs<WH, DEBUGHOOKINFO>> _debugHookEvent;
        private static EventHandler<Win32HookEventArgs<UnusedParam, UnusedParam>> _foregroundIdleHookEvent;
        private static EventHandler<Win32HookEventArgs<PM, MSG>> _getMsgHookEvent;
        private static EventHandler<Win32HookEventArgs<UnusedParam, EVENTMSG>> _journalPlaybackHookEvent; 
        private static EventHandler<Win32HookEventArgs<UnusedParam, EVENTMSG>> _journalRecordHookEvent; 
        private static EventHandler<Win32HookEventArgs<Keys, KeystrokeMessageFlags>> _keyboardHookEvent;
        private static EventHandler<Win32HookEventArgs<WM, KBDLLHOOKSTRUCT>> _lowLevelKeyboardHookEvent;
        private static EventHandler<Win32HookEventArgs<WM, MSLLHOOKSTRUCT >> _lowLevelMouseHookEvent;
        private static EventHandler<Win32HookEventArgs<WM, MOUSEHOOKSTRUCT>> _mouseHookEvent;
        private static EventHandler<Win32HookEventArgs<HSHELL, object, object>> _shellHookEvent;
        #endregion

        #region Properties
        // Dictionary which stores references to registered event handlers with their corresponding Hook Handles and callbacks.
        // The callbacks are also stored in order to keep a reference to them to prevent the garbage collector 
        // from collecting the callback after it is passed into unmanaged code.
        private static readonly Dictionary<object, Tuple<IntPtr, HOOKPROC>> RegisteredHooks = new Dictionary<object, Tuple<IntPtr, HOOKPROC>>();
        #endregion

        #region Constructor
        static Win32Hooks()
        {
            
        }
        #endregion

        #region Event Implementations


        /// <summary>
        /// The system calls this function before calling the window procedure to process a message sent to the thread.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<CurrentThreadIndicator, CWPSTRUCT>> CallWndHookEvent
        {
            add
            {
                value.RegisterHook(WH.CALLWNDPROC);
                _callWndHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _callWndHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function after the SendMessage function is called. 
        /// The hook procedure can examine the message; it cannot modify it.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<CurrentThreadIndicator, CWPRETSTRUCT>> CallWndRetHookEvent
        {
            add
            {
                value.RegisterHook(WH.CALLWNDPROCRET);
                _callWndRetHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _callWndRetHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function before activating, creating, destroying, minimizing, maximizing, moving, or sizing a window; 
        /// before completing a system command; before removing a mouse or keyboard event from the system message queue; 
        /// before setting the keyboard focus; or before synchronizing with the system message queue. 
        /// A computer-based training (CBT) application uses this hook procedure to receive useful notifications from the system.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<object, object>> CBTHookEvent
        {
            add
            {
                // CBT requires a special HookProc
                HOOKPROC hookProc = CbtHookProc;
                var hookHandle = SetHook((int)WH.CBT, hookProc);

                if (hookHandle == IntPtr.Zero)
                {
                    throw new Win32Exception(GetLastError());
                }

                RegisteredHooks.Add(value, new Tuple<IntPtr, HOOKPROC>(hookHandle, hookProc));
                _cbtHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _cbtHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function before calling the hook procedures associated with any type of hook. 
        /// The system passes information about the hook to be called to the DebugProc hook procedure, 
        /// which examines the information and determines whether to allow the hook to be called.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<WH, DEBUGHOOKINFO>> DebugHookEvent
        {
            add
            {
                value.RegisterHook(WH.DEBUG);
                _debugHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _debugHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function whenever the foreground thread is about to become idle.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<UnusedParam, UnusedParam>> ForegroundIdleHookEvent
        {
            add
            {
                value.RegisterHook(WH.FOREGROUNDIDLE);
                _foregroundIdleHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _foregroundIdleHookEvent -= value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<PM, MSG>> GetMsgHookEvent
        {
            add
            {
                value.RegisterHook(WH.GETMESSAGE);
                _getMsgHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _getMsgHookEvent -= value;
            }
        }

        /// <summary>
        /// Typically, an application uses this function to play back a series of mouse and keyboard messages recorded previously by the JournalRecordProc hook procedure. 
        /// As long as a JournalPlaybackProc hook procedure is installed, regular mouse and keyboard input is disabled.
        /// <remarks>
        /// See <see cref="http://www.wintellect.com/blogs/jrobbins/so-you-want-to-set-a-windows-journal-recording-hook-on-vista-it-s-not-nearly-as-easy-as-you-think"/> 
        /// for more information on using this hook on Windoes Vista and higher.
        /// </remarks>
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<UnusedParam, EVENTMSG>> JournalPlaybackHookEvent
        {
            add
            {
                // Playback requires a special HookProc
                HOOKPROC hookProc = JournalPlaybackHookProc;
                var hookHandle = SetHook((int)WH.JOURNALPLAYBACK, hookProc);

                if (hookHandle == IntPtr.Zero)
                {
                    throw new Win32Exception(GetLastError());
                }

                RegisteredHooks.Add(value, new Tuple<IntPtr, HOOKPROC>(hookHandle, hookProc));
                _journalPlaybackHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _journalPlaybackHookEvent -= value;
            }
        }
        /// <summary>
        /// The function records messages the system removes from the system message queue. 
        /// Later, an application can use a JournalPlaybackProc hook procedure to play back the messages.
        /// <remarks>
        /// See <see cref="http://www.wintellect.com/blogs/jrobbins/so-you-want-to-set-a-windows-journal-recording-hook-on-vista-it-s-not-nearly-as-easy-as-you-think"/> 
        /// for more information on using this hook on Windoes Vista and higher.
        /// </remarks>
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<UnusedParam, EVENTMSG>> JournalRecordHookEvent
        {
            add
            {
                value.RegisterHook(WH.JOURNALRECORD);
                _journalRecordHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _journalRecordHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function whenever an application calls the GetMessage or PeekMessage function and there is a keyboard message (WM_KEYUP or WM_KEYDOWN) to be processed.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<Keys, KeystrokeMessageFlags>> KeyboardHookEvent
        {
            add
            {
                value.RegisterHook(WH.KEYBOARD);
                _keyboardHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _keyboardHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function every time a new keyboard input event is about to be posted into a thread input queue.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<WM, KBDLLHOOKSTRUCT>> LowLevelKeyboardHookEvent
        {
            add
            {
                value.RegisterHook(WH.KEYBOARD_LL);
                _lowLevelKeyboardHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _lowLevelKeyboardHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function every time a new mouse input event is about to be posted into a thread input queue.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<WM, MSLLHOOKSTRUCT >> LowLevelMouseHookEvent
        {
            add
            {
                value.RegisterHook(WH.MOUSE_LL);
                _lowLevelMouseHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _lowLevelMouseHookEvent -= value;
            }
        }

        /// <summary>
        /// The system calls this function whenever an application calls the GetMessage or PM function and there is a mouse message to be processed.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<WM, MOUSEHOOKSTRUCT>> MouseHookEvent
        {
            add
            {
                value.RegisterHook(WH.MOUSE);
                _mouseHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _mouseHookEvent -= value;
            }
        }

        /// <summary>
        /// The function receives notifications of Shell events from the system.
        /// </summary>
        public static event EventHandler<Win32HookEventArgs<HSHELL, object, object>> ShellHookEvent
        {
            add
            {
                // Shell requires a special HookProc
                HOOKPROC hookProc = ShellHookProc;
                var hookHandle = SetHook((int)WH.SHELL, hookProc);

                if (hookHandle == IntPtr.Zero)
                {
                    throw new Win32Exception(GetLastError());
                }

                RegisteredHooks.Add(value, new Tuple<IntPtr, HOOKPROC>(hookHandle, hookProc));
                _shellHookEvent += value;
            }
            remove
            {
                value.UnregisterHook();
                _shellHookEvent -= value;
            }
        }
        #endregion

        private static void RegisterHook<TwParam, TlParam>(this EventHandler<Win32HookEventArgs<TwParam, TlParam>> value, WH hookId)
            where TwParam : new()
            where TlParam : new()
        {
            HOOKPROC hookProc = HookProc<TwParam, TlParam>;
            var hookHandle = SetHook((int)hookId, hookProc);

            if (hookHandle == IntPtr.Zero)
            {
                throw new Win32Exception(GetLastError());
            }

            RegisteredHooks.Add(value, new Tuple<IntPtr, HOOKPROC>(hookHandle, hookProc));
        }

        private static void UnregisterHook<TwParam, TlParam>(this EventHandler<Win32HookEventArgs<TwParam, TlParam>> value)
        {
            var hook = RegisteredHooks[value];
            UnhookWindowsHookEx(hook.Item1);
            RegisteredHooks.Remove(value);
        }

        private static void UnregisterHook<TnCode, TwParam, TlParam>(this EventHandler<Win32HookEventArgs<TnCode, TwParam, TlParam>> value)
        {
            var hook = RegisteredHooks[value];
            UnhookWindowsHookEx(hook.Item1);
            RegisteredHooks.Remove(value);
        }

        private static IntPtr SetHook(int idHook, HOOKPROC proc)
        {
            using (var curProcess = Process.GetCurrentProcess())
            using (var curModule = curProcess.MainModule)
            {
                // Get the Handle to the current module.
                var moduleHandle = GetModuleHandle(curModule.ModuleName);

                // Get the current thread ID. For Low Level / Journal (Global) hooks, this should be 0.
                // Note that in a managed environment, only the Keyboard and Mouse low level hooks and the Journal hooks are able to be global. See: http://support.microsoft.com/kb/318804
                var currentThreadId = ((idHook == (int) WH.KEYBOARD_LL) ||
                                       (idHook == (int) WH.MOUSE_LL) ||
                                       (idHook == (int) WH.JOURNALRECORD) ||
                                       (idHook == (int) WH.JOURNALPLAYBACK))
                    ? 0
                    : GetCurrentThreadId();

                return SetWindowsHookEx(idHook, proc, moduleHandle, currentThreadId);
            }
        }


        #region HookProcs

        /// <summary>
        /// The templated HookProc implementation used for most hooks.
        /// </summary>
        /// <typeparam name="TwParam">The type of the wParam of the Hook</typeparam>
        /// <typeparam name="TlParam">The type of the lParam of the Hook</typeparam>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        private static IntPtr HookProc<TwParam, TlParam>(int nCode, IntPtr wParam, IntPtr lParam) where TwParam : new() where TlParam : new()
        {
            if (nCode >= 0)
            {
                TwParam wParamHookStruct;
                TlParam lParamHookStruct;

                // Marshal the wParam to the correct type.
                if (typeof(TwParam) == typeof(UnusedParam))
                {
                    wParamHookStruct = new TwParam();
                }
                else if (typeof (TwParam).IsEnum)
                {
                    wParamHookStruct = (TwParam) Enum.Parse(typeof (TwParam), wParam.ToString());
                }
                else
                {
                    wParamHookStruct = (TwParam) Marshal.PtrToStructure(wParam, typeof (TwParam));
                }

                // Catch cases where the lParam is actually an int instead of an IntPtr 
                // and convert it to and IntPtr so that it can then be marshaled to the correct struct type.
                if (typeof(TlParam) == typeof(KeystrokeMessageFlags))
                {
                    // Save the lParams actual value in a temp variable
                    var value = (int) lParam;

                    // Allocate an actual IntPtr of the required size
                    lParam = Marshal.AllocHGlobal(sizeof(int));

                    // Marshal the value into the memory space that was just created
                    Marshal.WriteInt32(lParam, value);
                }

                // Marshal the lParam to the correct type.
                if (typeof (TlParam) == typeof (UnusedParam))
                {
                    lParamHookStruct = new TlParam();
                }
                else
                {
                    lParamHookStruct = (TlParam)Marshal.PtrToStructure(lParam, typeof(TlParam));   
                }          

                // Run time-consuming actions and delegate invocation on a new thread to prevent the hook timing out for low level hooks.
                // See: http://msdn.microsoft.com/en-us/library/windows/desktop/ms644986(v=vs.85).aspx
                // "The hook procedure should process a message in less time than the data entry specified in the LowLevelHooksTimeout value in the following registry key:
                //  HKEY_CURRENT_USER\Control Panel\Desktop
                //  The value is in milliseconds. 
                //  If the hook procedure times out, the system passes the message to the next hook. 
                //  However, on Windows 7 and later, the hook is silently removed without being called. 
                //  There is no way for the application to know whether the hook is removed."
                Task.Run(() =>
                {
                    // Construct the event args
                    var eventArgs = new Win32HookEventArgs<TwParam, TlParam>(nCode, wParamHookStruct, lParamHookStruct);

                    // Find the delegate belonging to the event which accepts the eventargs with the TwParam and TlParam types
                    var hookEventBackingFieldDelegate = typeof(Win32Hooks)                                      // Of this static class
                        .GetFields(BindingFlags.NonPublic | BindingFlags.Static)                                // Get all private static fields (this includes the event handler we are looking for)
                        .Single(x => x.FieldType == typeof(EventHandler<Win32HookEventArgs<TwParam, TlParam>>)) // Find the one field which type matches the type we are looking for
                        .GetValue(null) as MulticastDelegate;                                                   // Get the field value, which is a MulticastDelegate

                    if (hookEventBackingFieldDelegate == null)
                    {
                        throw new Exception("The requested event delegate does not exist.");
                    }
                    // Invoke all attached delegates
                    foreach (var dlg in hookEventBackingFieldDelegate.GetInvocationList())
                    {
                        // Invoke the delegate passing it a sender of null (since it is sent from a static class) and the event args
                        dlg.Method.Invoke(dlg.Target, new object[] { null, eventArgs });
                    }
                }).Wait();
            }
            return CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);
        }

        /// <summary>
        /// The HookProc for the CBTHook. See <see cref="http://msdn.microsoft.com/en-us/library/windows/desktop/ms644977(v=vs.85).aspx"/>
        /// The value returned by the hook procedure determines whether the system allows or prevents one of these operations. 
        /// For operations corresponding to the following CBT hook codes, the return value must be 0 to allow the operation, or 1 to prevent it.
        /// HCBT_ACTIVATE
        /// HCBT_CREATEWND
        /// HCBT_DESTROYWND
        /// HCBT_MINMAX
        /// HCBT_MOVESIZE
        /// HCBT_SETFOCUS
        /// HCBT_SYSCOMMAND
        /// For operations corresponding to the following CBT hook codes, the return value is ignored.
        /// HCBT_CLICKSKIPPED
        /// HCBT_KEYSKIPPED
        /// HCBT_QS
        /// </summary>
        /// <param name="nCode">The code that the hook procedure uses to determine how to process the message. 
        /// If nCode is less than zero, the hook procedure must pass the message to the CallNextHookEx function without further processing and should return the value returned by CallNextHookEx. 
        /// </param>
        /// <param name="wParam">Depends on the nCode parameter.</param>
        /// <param name="lParam">Depends on the nCode parameter.</param>
        /// <returns>0 to allow the operation, or 1 to prevent it.</returns>
        private static IntPtr CbtHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            object wParamHookStruct, lParamHookStruct;

            switch ((HCBT) nCode)
            {
                case HCBT.ACTIVATE:

                    // Specifies the handle to the window about to be activated.
                    wParamHookStruct = wParam;

                    // Specifies a long pointer to a CBTACTIVATESTRUCT structure containing the handle
                    // to the active window and specifies whether the activation is changing because of a mouse click.
                    lParamHookStruct = Marshal.PtrToStructure(lParam, typeof(CBTACTIVATESTRUCT));

                    break;

                case HCBT.CLICKSKIPPED:

                    // Specifies the mouse message removed from the system message queue.
                    wParamHookStruct = Enum.Parse(typeof(WM), wParam.ToString());

                    // Specifies a long pointer to a MOUSEHOOKSTRUCT structure containing the hit-test code and the handle to the window for which the mouse message is intended.
                    // The HCBT_CLICKSKIPPED value is sent to a CBTProc hook procedure only if a WH_MOUSE hook is installed. For a list of hit-test codes, see WM_NCHITTEST.
                    lParamHookStruct = Marshal.PtrToStructure(lParam, typeof(MOUSEHOOKSTRUCT));

                    break;

                case HCBT.CREATEWND:

                    // Specifies the handle to the new window.
                    wParamHookStruct = wParam;

                    // Specifies a long pointer to a CBT_CREATEWND structure containing initialization parameters for the window. 
                    // The parameters include the coordinates and dimensions of the window. 
                    // By changing these parameters, a CBTProc hook procedure can set the initial size and position of the window.
                    lParamHookStruct = Marshal.PtrToStructure(lParam, typeof(CBT_CREATEWND));

                    break;

                case HCBT.DESTROYWND:

                    // Specifies the handle to the window about to be destroyed.
                    wParamHookStruct = wParam;

                    // Is undefined and must be set to zero.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HCBT.KEYSKIPPED:

                    // Specifies the virtual-key code.
                    wParamHookStruct = Enum.Parse(typeof(Keys), wParam.ToString());

                    //Specifies the repeat count, scan code, key-transition code, previous key state, and context code. 
                    // The HCBT_KEYSKIPPED value is sent to a CBTProc hook procedure only if a WH_KEYBOARD hook is installed. 
                    // For more information, see WM_KEYUP or WM_KEYDOWN.
                    lParamHookStruct = new KeystrokeMessageFlags((uint) lParam.ToInt32());

                    break;

                case HCBT.MINMAX:

                    // Specifies the handle to the window being minimized or maximized.
                    wParamHookStruct = wParam;

                    // Specifies, in the low-order word, a show-window value (SW_) specifying the operation. 
                    // For a list of show-window values, see the ShowWindow. 
                    // The high-order word is undefined.
                    lParamHookStruct = Enum.Parse(typeof(SW), lParam.ToString());

                    break;

                case HCBT.MOVESIZE:

                    // Specifies the handle to the window to be moved or sized.
                    wParamHookStruct = wParam;

                    // Specifies a long pointer to a RECT structure containing the coordinates of the window. 
                    // By changing the values in the structure, a CBTProc hook procedure can set the final coordinates of the window.
                    lParamHookStruct = Marshal.PtrToStructure(lParam, typeof(RECT));

                    break;

                case HCBT.QS:

                    // Is undefined and must be zero.
                    wParamHookStruct = new UnusedParam();

                    // Is undefined and must be zero.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HCBT.SETFOCUS:

                    // Specifies the handle to the window gaining the keyboard focus.
                    wParamHookStruct = wParam;

                    // Specifies the handle to the window losing the keyboard focus.
                    lParamHookStruct = lParam;

                    break;

                case HCBT.SYSCOMMAND:

                    //In WM_SYSCOMMAND messages, the four low-order bits of the wParam parameter are used internally by the system. 
                    // To obtain the correct result when testing the value of wParam, an application must combine the value 0xFFF0 with the wParam value by using the bitwise AND operator.
                    var realWParam = wParam.ToInt32() & 0x0000FFF0;

                    // Specifies a system-command value (SC_) specifying the system command. 
                    // For more information about system-command values, see http://msdn.microsoft.com/en-us/library/windows/desktop/ms646360(v=vs.85).aspx.
                    wParamHookStruct = Enum.Parse(typeof(SC), realWParam.ToString(CultureInfo.InvariantCulture));

                    // Contains the same data as the lParam value of a WM_SYSCOMMAND message: 
                    // If a system menu command is chosen with the mouse, the low-order word contains the x-coordinate of the cursor, 
                    // in screen coordinates, and the high-order word contains the y-coordinate; otherwise, the parameter is not used.
                    lParamHookStruct = new CBT_SYSCOMMAND(lParam.ToInt32());
                    break;

                default:
                    return CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);

            }
  
            // Construct the event args
            var eventArgs = new Win32HookEventArgs<object, object>(nCode, wParamHookStruct, lParamHookStruct);

            // Invoke the delegate passing it a sender of null (since it is sent from a static class) and the event args
            _cbtHookEvent.Invoke(null, eventArgs);

            return eventArgs.ReturnValue != null ? (IntPtr) eventArgs.ReturnValue : CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);

        }

        /// <summary>
        /// The HookProc for the JournalPlaybackHook.
        /// <remarks>
        /// I have no way to test if this thing actually works because this setting uiAccess=true in the application manifest. 
        /// This however requires some special authorization from Microsoft. 
        /// See: <see cref="http://www.pcreview.co.uk/forums/journal-playback-hook-t3018085.html"/>
        /// </remarks>
        /// </summary>
        /// <param name="nCode">A code the hook procedure uses to determine how to process the message. 
        /// If code is less than zero, the hook procedure must pass the message to the CallNextHookEx function without further processing and should return the value returned by CallNextHookEx. 
        /// This parameter can be one of the following values.</param>
        /// <param name="wParam">This parameter is not used.</param>
        /// <param name="lParam">A pointer to an EVENTMSG structure that represents a message being processed by the hook procedure. 
        /// This parameter is valid only when the code parameter is HC_GETNEXT.</param>
        /// <returns></returns>
        private static IntPtr JournalPlaybackHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            switch ((HC)nCode)
            {
                case HC.GETNEXT:

                    var wParamHookStruct = wParam.MarshalTo<UnusedParam>();
                    var lParamHookStruct = lParam.MarshalTo<EVENTMSG>();

                    // Construct the event args
                    var eventArgs = new Win32HookEventArgs<UnusedParam, EVENTMSG>(nCode, wParamHookStruct, lParamHookStruct);

                    // Invoke the delegate passing it a sender of null (since it is sent from a static class) and the event args
                    _journalPlaybackHookEvent.Invoke(null, eventArgs);
              
                    // Copy the returned EVENTMSG struct back into the lParam
                    Marshal.StructureToPtr(eventArgs.LParam,lParam,true);

                    break;

                case HC.NOREMOVE:
                    // ????
                    break;

                case HC.SKIP:
                case HC.SYSMODALOFF:
                case HC.SYSMODALON:

                    // Invoke the delegate passing it a sender of null (since it is sent from a static class) and the event args
                    _journalPlaybackHookEvent.Invoke(null, new Win32HookEventArgs<UnusedParam, EVENTMSG>(nCode, new UnusedParam(), new EVENTMSG()));

                    break;

                default:
                    return CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);
            }

            return CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);

        }

        /// <summary>
        /// The HookProc for the ShellHook. See <see cref="http://msdn.microsoft.com/en-us/library/windows/desktop/ms644991(v=vs.85).aspx"/>
        /// </summary>
        /// <param name="nCode">The hook code. If nCode is less than zero, the hook procedure must pass the message to the 
        /// CallNextHookEx function without further processing and should return the value returned by CallNextHookEx.</param>
        /// <param name="wParam">This parameter depends on the value of the nCode parameter.</param>
        /// <param name="lParam">This parameter depends on the value of the nCode parameter.</param>
        /// <returns>The return value should be zero unless the value of nCode is HSHELL_APPCOMMAND and the shell procedure handles the WM_COMMAND message. 
        /// In this case, the return should be nonzero.</returns>
        private static IntPtr ShellHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            object wParamHookStruct, lParamHookStruct;
            switch ((HSHELL)nCode)
            {
                case HSHELL.ACCESSIBILITYSTATE:

                    // Indicates which accessibility feature has changed state. 
                    // This value is one of the following: ACCESS_FILTERKEYS, ACCESS_MOUSEKEYS, or ACCESS_STICKYKEYS.
                    wParamHookStruct = Enum.Parse(typeof(ACCESS), wParam.ToString());

                    // This parameter is not used.
                    lParamHookStruct = new UnusedParam();
                   
                    break;

                case HSHELL.ACTIVATESHELLWINDOW:

                    // This parameter is not used.
                    wParamHookStruct = new UnusedParam();

                    // This parameter is not used.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HSHELL.APPCOMMAND:

                    // Indicates where the WM_APPCOMMAND message was originally sent; for example, the handle to a window. 
                    // For more information, see cmd parameter in WM_APPCOMMAND.
                    wParamHookStruct = wParam;

                    // GET_APPCOMMAND_LPARAM(lParam) is the application command corresponding to the input event.
                    // GET_DEVICE_LPARAM(lParam) indicates what generated the input event; for example, the mouse or keyboard. 
                    // For more information, see the uDevice parameter description under WM_APPCOMMAND.
                    // GET_FLAGS_LPARAM(lParam) depends on the value of cmd in WM_APPCOMMAND. 
                    // For example, it might indicate which virtual keys were held down when the WM_APPCOMMAND message was originally sent. 
                    // For more information, see the dwCmdFlags description parameter under WM_APPCOMMAND.
                    // TODO: Create a struct which can parse this easily
                    lParamHookStruct = lParam;

                    break;

                case HSHELL.GETMINRECT:

                    // A handle to the minimized or maximized window.
                    wParamHookStruct = wParam;

                    // A pointer to a RECT structure.
                    lParamHookStruct = lParam.MarshalTo<RECT>();

                    break;

                case HSHELL.LANGUAGE:

                    // A handle to the window.
                    wParamHookStruct = wParam;

                    // A handle to a keyboard layout.
                    // TODO: Find out how to cast this to an ENUM or something?
                    lParamHookStruct = lParam;

                    break;

                case HSHELL.REDRAW:

                    // A handle to the redrawn window.
                    wParamHookStruct = wParam;

                    // The value is TRUE if the window is flashing, or FALSE otherwise.
                    lParamHookStruct = lParam.ToInt32() != 0;

                    break;

                case HSHELL.TASKMAN:

                    // This parameter is not used.
                    wParamHookStruct = new UnusedParam();

                    // This parameter is not used.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HSHELL.WINDOWACTIVATED:

                    // A handle to the activated window.
                    wParamHookStruct = wParam;

                    // The value is TRUE if the window is in full-screen mode, or FALSE otherwise.
                    lParamHookStruct = lParam.ToInt32() != 0;

                    break;

                case HSHELL.WINDOWCREATED:

                    // A handle to the created window.
                    wParamHookStruct = wParam;

                    // This parameter is not used.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HSHELL.WINDOWDESTROYED:

                    // A handle to the destroyed window.
                    wParamHookStruct = wParam;

                    // This parameter is not used.
                    lParamHookStruct = new UnusedParam();

                    break;

                case HSHELL.WINDOWREPLACED:

                    // A handle to the window being replaced. Windows 2000:  Not supported.
                    wParamHookStruct = wParam;

                    // A handle to the new window. Windows 2000:  Not supported.
                    lParamHookStruct = lParam;

                    break;
                default:
                    return CallNextHookEx(IntPtr.Zero, nCode, wParam, lParam);
            }

            // Construct the event args
            var eventArgs = new Win32HookEventArgs<HSHELL, object, object>((HSHELL) nCode, wParamHookStruct, lParamHookStruct);

            // Invoke the delegate passing it a sender of null (since it is sent from a static class) and the event args
            _shellHookEvent.Invoke(null, eventArgs);

            // The return value should be zero unless the value of nCode is HSHELL_APPCOMMAND and the shell procedure handles the WM_COMMAND message. In this case, the return should be nonzero.
            return ((HSHELL)nCode == HSHELL.APPCOMMAND && eventArgs.ReturnValue != null) ? (IntPtr)eventArgs.ReturnValue : IntPtr.Zero;

        }
        #endregion
    }
}