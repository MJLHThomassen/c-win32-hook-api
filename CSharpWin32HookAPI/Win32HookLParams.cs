﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 03-01-2015
 * 
 */

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CSharpWin32HookAPI
{
    #region Helper Structs

    /// <summary>
    /// Defines the x- and y- coordinates of a point.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        /// <summary>
        /// The x-coordinate of the point.
        /// </summary>
        public int x;

        /// <summary>
        /// The y-coordinate of the point.
        /// </summary>
        public int y;
    }

    /// <summary>
    /// Defines the coordinates of the upper-left and lower-right corners of a rectangle.
    /// 
    /// <remarks>
    /// By convention, the right and bottom edges of the rectangle are normally considered exclusive. 
    /// In other words, the pixel whose coordinates are ( right, bottom ) lies immediately outside of the rectangle.
    /// For example, when RECT is passed to the FillRect function, the rectangle is filled up to, but not including, 
    /// the right column and bottom row of pixels. This structure is identical to the RECTL structure.
    /// </remarks>
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        /// <summary>
        /// The x-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int left;

        /// <summary>
        /// The y-coordinate of the upper-left corner of the rectangle.
        /// </summary>
        public int top;

        /// <summary>
        /// The x-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int right;

        /// <summary>
        /// The y-coordinate of the lower-right corner of the rectangle.
        /// </summary>
        public int bottom;
    }

    /// <summary>
    /// Specifies whether the message was sent by the current thread.
    /// If the message was sent by the current thread, it is nonzero; otherwise, it is zero. 
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct CurrentThreadIndicator
    {
        [FieldOffset(0)]
        private readonly uint _flags;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="flags">The value indicating the message thread origin.</param>
        public CurrentThreadIndicator(uint flags)
        {
            _flags = flags;
        }

        /// <summary>
        /// True: The message was sent by the current thread.
        /// False: The message was not sent by the current thread.
        /// </summary>
        public bool IsNonZero
        {
            get
            {
                return _flags != 0;
            }
        }
    }

    /// <summary>
    /// Defines the initialization parameters passed to the window procedure of an application. 
    /// These members are identical to the parameters of the CreateWindowEx function.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CREATESTRUCT
    {
        /// <summary>
        /// Contains additional data which may be used to create the window. 
        /// If the window is being created as a result of a call to the CreateWindow or CreateWindowEx function, 
        /// this member contains the value of the lpParam parameter specified in the function call.
        /// If the window being created is a MDI client window, this member contains a pointer to a CLIENTCREATESTRUCT structure. 
        /// If the window being created is a MDI child window, this member contains a pointer to an MDICREATESTRUCT structure.
        /// If the window is being created from a dialog template, this member is the address of a SHORT value that specifies the size, in bytes, of the window creation data. 
        /// The value is immediately followed by the creation data. For more information, see the following Remarks section.
        /// <remarks>
        /// Because the lpszClass member can contain a pointer to a local (and thus inaccessable) atom, do not obtain the class name by using this member. 
        /// Use the GetClassName function instead.
        /// You should access the data represented by the lpCreateParams member using a pointer that has been declared using the UNALIGNED type, 
        /// because the pointer may not be DWORD aligned. This is demonstrated in the following example:
        /// <code>
        /// typedef struct tagMyData 
        /// {
        ///    // Define creation data here. 
        /// } MYDATA; 
        /// 
        /// typedef struct tagMyDlgData 
        /// { 
        ///     SHORT   cbExtra; 
        ///     MYDATA  myData; 
        /// } MYDLGDATA, UNALIGNED *PMYDLGDATA; 
        ///  
        /// PMYDLGDATA pMyDlgdata = (PMYDLGDATA) (((LPCREATESTRUCT) lParam)->lpCreateParams);
        /// </code>
        /// </remarks>
        /// </summary>
        public IntPtr lpCreateParams;

        /// <summary>
        /// A handle to the module that owns the new window.
        /// </summary>
        public IntPtr hInstance;

        /// <summary>
        /// A handle to the menu to be used by the new window.
        /// </summary>
        public IntPtr hMenu;

        /// <summary>
        /// A handle to the parent window, if the window is a child window. 
        /// If the window is owned, this member identifies the owner window. 
        /// If the window is not a child or owned window, this member is NULL.
        /// </summary>
        public IntPtr hwndParent;

        /// <summary>
        /// The height of the new window, in pixels.
        /// </summary>
        public int cy;

        /// <summary>
        /// The width of the new window, in pixels.
        /// </summary>
        public int cx;

        /// <summary>
        /// The y-coordinate of the upper left corner of the new window. 
        /// If the new window is a child window, coordinates are relative to the parent window.
        /// Otherwise, the coordinates are relative to the screen origin.
        /// </summary>
        public int y;

        /// <summary>
        /// The x-coordinate of the upper left corner of the new window. 
        /// If the new window is a child window, coordinates are relative to the parent window. 
        /// Otherwise, the coordinates are relative to the screen origin.
        /// </summary>
        public int x;

        /// <summary>
        /// The style for the new window. For a list of possible values, see 
        /// <see cref="http://msdn.microsoft.com/en-us/library/windows/desktop/ms632600(v=vs.85).aspx">Window Styles</see>.
        /// </summary>
        public WS style;

        /// <summary>
        /// The name of the new window.
        /// </summary>
        public IntPtr lpszName;

        /// <summary>
        /// A pointer to a null-terminated string or an atom that specifies the class name of the new window.
        /// </summary>
        public IntPtr lpszClass;

        /// <summary>
        /// The extended window style for the new window. For a list of possible values, see 
        /// <see cref="http://msdn.microsoft.com/en-us/library/windows/desktop/ff700543(v=vs.85).aspx">Extended Window Styles</see>.
        /// </summary>
        public int dwExStyle;
    }

    /// <summary>
    /// A struct for applying hook events in a typesafe way when the parameter itself is not used 
    /// (such as in the Foreground Idle hook where both the lParam and wParam of the callback are unused).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct UnusedParam
    {
    }
    #endregion

    #region LParam Structs


    /// <summary>
    /// Defines the message parameters passed to a WH_CALLWNDPROC hook procedure, CallWndProc.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CWPSTRUCT
    {
        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public IntPtr lParam;

        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public IntPtr wParam;

        /// <summary>
        /// The message.
        /// </summary>
        public uint message;

        /// <summary>
        /// A handle to the window to receive the message.
        /// </summary>
        public IntPtr hwnd;

    }

    /// <summary>
    /// Defines the message parameters passed to a WH_CALLWNDPROCRET hook procedure, CallWndRetProc.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CWPRETSTRUCT
    {
        /// <summary>
        /// The return value of the window procedure that processed the message specified by the message value.
        /// </summary>
        public IntPtr lResult;

        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public IntPtr lParam;

        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public IntPtr wParam;

        /// <summary>
        /// The message.
        /// </summary>
        public uint message;

        /// <summary>
        /// A handle to the window that processed the message specified by the message value.
        /// </summary>
        public IntPtr hwnd;

    }

    /// <summary>
    /// Contains information passed to a WH_CBT hook procedure, CBTProc, before a window is activated.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CBTACTIVATESTRUCT
    {
        /// <summary>
        /// This member is TRUE if a mouse click is causing the activation or FALSE if it is not.
        /// </summary>
        public bool fMouse;

        /// <summary>
        /// A handle to the active window.
        /// </summary>
        public IntPtr hWndActive;
    }

    /// <summary>
    /// Contains information passed to a WH_CBT hook procedure, CBTProc, before a window is created.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CBT_CREATEWND
    {
        /// <summary>
        /// A pointer to a CREATESTRUCT structure that contains initialization parameters for the window about to be created.
        /// </summary>
        public IntPtr lpcs;

        /// <summary>
        /// A handle to the window whose position in the Z order precedes that of the window being created. This member can also be NULL.
        /// </summary>
        public IntPtr hwndInsertAfter;
    }

    /// <summary>
    /// Contains information passed to a WH_CBT hook procedure, CBTProc, when a HCBT_SYSCOMMAND is recieved.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CBT_SYSCOMMAND
    {
        private readonly int _value;

        public CBT_SYSCOMMAND(int value)
        {
            _value = value;
        }

        /// <summary>
        /// Specifies the horizontal position of the cursor, in screen coordinates, if a window menu command is chosen with the mouse. 
        /// Otherwise, this parameter is not used.
        /// </summary>
        public int X
        {
            get
            {
                return _value & 0x0000FFFF;
            }
        }

        /// <summary>
        /// Specifies the vertical position of the cursor, in screen coordinates, if a window menu command is chosen with the mouse. 
        /// This parameter is –1 if the command is chosen using a system accelerator, or zero if using a mnemonic.
        /// </summary>
        public int Y
        {
            get
            {
                return _value >> 16;
            }
        }

        /// <summary>
        /// If the wParam is SC_KEYMENU, lParam contains the character code of the key that is used with the ALT key to display the popup menu. 
        /// For example, pressing ALT+F to display the File popup will cause a WM_SYSCOMMAND with wParam equal to SC_KEYMENU and lParam equal to 'f'.
        /// This property is only valid when the command is of type SC_KEYMENU..
        /// </summary>
        public char CharacterCode
        {
            get
            {
                return (char) _value;
            }
        }

        /// <summary>
        /// The display is powering on.
        /// This property is only valid when the command is of type SC_MONITORPOWER.
        /// </summary>
        public bool IsDisplayPoweringOn
        {
            get
            {
                return _value == -1;
            }
        }

        /// <summary>
        /// The display is going to low power.
        /// This property is only valid when the command is of type SC_MONITORPOWER.
        /// </summary>
        public bool IsDisplayGoingToLowPower
        {
            get
            {
                return _value == 1;
            }
        }

        /// <summary>
        /// The display is being shut off.
        /// This property is only valid when the command is of type SC_MONITORPOWER.
        /// </summary>
        public bool IsDisplayBeingShutOff
        {
            get
            {
                return _value == 2;
            }
        }
    }

    /// <summary>
    /// Contains debugging information passed to a WH_DEBUG hook procedure, DebugProc.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DEBUGHOOKINFO 
    {
        /// <summary>
        /// A handle to the thread containing the filter function.
        /// </summary>
        public int idThread;

        /// <summary>
        /// A handle to the thread that installed the debugging filter function.
        /// </summary>
        public int idThreadInstaller;

        /// <summary>
        /// The value to be passed to the hook in the lParam parameter of the DebugProc callback function.
        /// </summary>
        public IntPtr lParam;

        /// <summary>
        /// The value to be passed to the hook in the wParam parameter of the DebugProc callback function.
        /// </summary>
        public IntPtr wParam;

        /// <summary>
        /// The value to be passed to the hook in the nCode parameter of the DebugProc callback function.
        /// </summary>
        public int code;

    }

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MSG
    {
        /// <summary>
        /// A handle to the window whose window procedure receives the message. 
        /// This member is NULL when the message is a thread message.
        /// </summary>
        public IntPtr hwnd;

        /// <summary>
        /// The message identifier. Applications can only use the low word; the high word is reserved by the system.
        /// </summary>
        public WM message;

        /// <summary>
        /// Additional information about the message. 
        /// The exact meaning depends on the value of the message member.
        /// </summary>
        public IntPtr wParam;

        /// <summary>
        /// Additional information about the message. 
        /// The exact meaning depends on the value of the message member.
        /// </summary>
        public IntPtr lParam;

        /// <summary>
        /// The time at which the message was posted.
        /// The time is a long integer that specifies the elapsed time, in milliseconds, 
        /// from the time the system was started to the time the message was created (that is, placed in the thread's message queue).
        /// </summary>
        public int time;

        /// <summary>
        /// The cursor position, in screen coordinates, when the message was posted.
        /// </summary>
        public POINT pt;
    }

    /// <summary>
    /// Contains information about a hardware message sent to the system message queue. 
    /// This structure is used to store message information for the JournalPlaybackProc callback function.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct EVENTMSG
    {
        /// <summary>
        /// The message.
        /// </summary>
        public WM message;

        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public uint paramL;

        /// <summary>
        /// Additional information about the message. The exact meaning depends on the message value.
        /// </summary>
        public uint paramH;

        /// <summary>
        /// The time at which the message was posted.
        /// </summary>
        public int time;

        /// <summary>
        /// A handle to the window to which the message was posted.
        /// </summary>
        public IntPtr hwnd;
    }

    /// <summary>
    /// Contains information about a low-level keyboard input event.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct KBDLLHOOKSTRUCT
    {
        /// <summary>
        /// A virtual-key code. The code must be a value in the range 1 to 254.
        /// </summary>
        public Keys vkCode;

        /// <summary>
        /// A hardware scan code for the key.
        /// </summary>
        public int scanCode;

        /// <summary>
        /// The extended-key flag, event-injected flags, context code, and transition-state flag. 
        /// </summary>
        public LowLevelKeyboardHookFlags flags;

        /// <summary>
        /// The time stamp for this message, equivalent to what GetMessageTime would return for this message.
        /// </summary>
        public int time;

        /// <summary>
        /// Additional information associated with the message.
        /// </summary>
        public IntPtr dwExtraInfo;
    }

    /// <summary>
    /// Contains information about a low-level mouse input event.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MSLLHOOKSTRUCT
    {
        /// <summary>
        /// The x- and y-coordinates of the cursor, in screen coordinates.
        /// </summary>
        public POINT pt;

        /// <summary>
        /// If the message is WM_MOUSEWHEEL, the high-order word of this member is the wheel delta. The low-order word is reserved. 
        /// A positive value indicates that the wheel was rotated forward, away from the user; 
        /// a negative value indicates that the wheel was rotated backward, toward the user. One wheel click is defined as WHEEL_DELTA, which is 120.
        /// 
        /// If the message is WM_XBUTTONDOWN, WM_XBUTTONUP, WM_XBUTTONDBLCLK, WM_NCXBUTTONDOWN, WM_NCXBUTTONUP, or WM_NCXBUTTONDBLCLK, 
        /// the high-order word specifies which X button was pressed or released, and the low-order word is reserved. 
        /// This value can be one or more of the following values. Otherwise, mouseData is not used.
        /// </summary>
        public int mouseData;

        /// <summary>
        /// The event-injected flags.
        /// </summary>
        public LowLevelMouseHookFlags flags;

        /// <summary>
        /// The time stamp for this message.
        /// </summary>
        public int time;

        /// <summary>
        /// Additional information associated with the message.
        /// </summary>
        public IntPtr dwExtraInfo;
    }

    /// <summary>
    /// Contains information about a mouse event passed to a WH_MOUSE hook procedure, MouseProc.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MOUSEHOOKSTRUCT 
    {
        /// <summary>
        /// The x- and y-coordinates of the cursor, in screen coordinates.
        /// </summary>
        public POINT pt;

        /// <summary>
        /// A handle to the window that will receive the mouse message corresponding to the mouse event.
        /// </summary>
        public IntPtr hwnd;

        /// <summary>
        /// The hit-test value. For a list of hit-test values, see the description of the WM_NCHITTEST message.
        /// </summary>
        public int wHitTestCode;

        /// <summary>
        /// Additional information associated with the message.
        /// </summary>
        public IntPtr dwExtraInfo;
    }

    #endregion

    #region Strongly-Typed Flag Struct Parameters

    [StructLayout(LayoutKind.Explicit)]
    public struct LowLevelKeyboardHookFlags
    {
        [FieldOffset(0)]
        private readonly uint _flags;

        public LowLevelKeyboardHookFlags(uint flags)
        {
            _flags = flags;
        }

        public bool IsExtendedKey
        {
            get
            {
                return (_flags & (uint)LowLevelKeyboardHookFlagMasks.LLKHF_EXTENDED) != 0;
            }
        }

        public bool IsInjectedFromLowerIntegrity
        {
            get
            {
                return (_flags & (uint)LowLevelKeyboardHookFlagMasks.LLKHF_LOWER_IL_INJECTED) != 0;
            }
        }

        public bool IsInjected
        {
            get
            {
                return (_flags & (uint)LowLevelKeyboardHookFlagMasks.LLKHF_INJECTED) != 0;
            }
        }

        public bool IsAltDown
        {
            get
            {
                return (_flags & (uint)LowLevelKeyboardHookFlagMasks.LLKHF_ALTDOWN) != 0;
            }
        }

        public bool IsKeyBeingReleased
        {
            get
            {
                return (_flags & (uint)LowLevelKeyboardHookFlagMasks.LLKHF_UP) != 0;
            }
        }

        public override string ToString()
        {
            var str = "";
            foreach (var property in GetType().GetProperties())
            {
                str += " " + property.Name + ": " + property.GetValue(this) + "\n";
            }

            return str;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct LowLevelMouseHookFlags
    {
        [FieldOffset(0)]
        private readonly uint _flags;

        public LowLevelMouseHookFlags(uint flags)
        {
            _flags = flags;
        }

        public bool IsInjected
        {
            get
            {
                return (_flags & (uint)LowLEvelMouseHookFlagMasks.LLMHF_INJECTED) != 0;
            }
        }

        public bool IsInjectedFromLowerIntegrity
        {
            get
            {
                return (_flags & (uint)LowLEvelMouseHookFlagMasks.LLMHF_LOWER_IL_INJECTED) != 0;
            }
        }

        public override string ToString()
        {
            var str = "";
            foreach (var property in GetType().GetProperties())
            {
                str += " " + property.Name + ": " + property.GetValue(this) + "\n";
            }

            return str;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct KeystrokeMessageFlags
    {
        [FieldOffset(0)]
        private readonly uint _flags;

        public KeystrokeMessageFlags(uint flags)
        {
            _flags = flags;
        }

        public uint RepeatCount
        {
            get
            {
                return _flags & (uint)KeystrokeMessageFlagMasks.REPEAT_COUNT;
            }
        }

        public uint ScanCode
        {
            get
            {
                return (_flags & (uint)KeystrokeMessageFlagMasks.SCAN_CODE) >> 16;
            }
        }

        public bool IsExtendedKey
        {
            get
            {
                return (_flags & (uint)KeystrokeMessageFlagMasks.EXTENDED_KEY) != 0;
            }
        }

        public bool IsAltDown
        {
            get
            {
                return (_flags & (uint)KeystrokeMessageFlagMasks.CONTEXT_CODE) != 0;
            }
        }

        public bool IsPreviousKeyStateDown
        {
            get
            {
                return (_flags & (uint)KeystrokeMessageFlagMasks.PREVIOUS_STATE) != 0;
            }
        }

        public bool IsKeyBeingReleased
        {
            get
            {
                return (_flags & (uint)KeystrokeMessageFlagMasks.TRANSITION_STATE) != 0;
            }
        }

        public override string ToString()
        {
            var str = "";
            foreach (var property in GetType().GetProperties())
            {
                str += " " + property.Name + ": " + property.GetValue(this) + "\n";
            }

            return str;
        }
    }

    #endregion
}
