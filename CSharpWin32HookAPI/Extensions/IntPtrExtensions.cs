﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 06-01-2015
 * 
 */

using System;
using System.Runtime.InteropServices;

namespace CSharpWin32HookAPI.Extensions
{
    /// <summary>
    /// Extension class for easy conversion of IntPtr's to the various Hook API structs.
    /// </summary>
    public static class IntPtrExtensions
    {

        /// <summary>
        /// Extension functions for marshalling an IntPtr to a struct of the given type.
        /// </summary>
        /// <typeparam name="T">The type of the struct.</typeparam>
        /// <param name="ptr">The IntPtr to marshal.</param>
        /// <returns>The marshalled IntPtr struct.</returns>
        public static T MarshalTo<T>(this IntPtr ptr)
        {
            return (T)Marshal.PtrToStructure(ptr, typeof(T));
        }

        public static dynamic MarshalDebugHookLParam(this IntPtr ptr, WH wParam)
        {
            switch (wParam)
            {
                case WH.GETMESSAGE:
                    return ptr.MarshalTo<MSG>();
                    break;

                case WH.MOUSE:
                    return ptr.MarshalTo<MOUSEHOOKSTRUCT>();
                    break;

                default:
                    return null;
                    break;

            }
        }

        public static CREATESTRUCT MarshalCbtCreateWndLpcs(this IntPtr ptr)
        {
            return ptr.MarshalTo<CREATESTRUCT>();
        }
    }
}
