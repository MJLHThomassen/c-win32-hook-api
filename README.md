# README #


## What is this repository for? ##

### Summary ###
The C#/CSharp Win32 Hook API is an interface /  wrapper for using the Win32 Hook API in C# or other .NET programs. The goal is to simplify using Win32 Hooks in .NET programs, mainly the keyboard and mouse hooks, by providing an interface to the API, without the need for end users to use PInvoke and maintaining object lifecycles.

The Native C Win32 Hook API is documented on [MSDN Hooks].

### Current Version ###
v0.1

### Release Changelog ###

* v0.2:
    * Most Hooks implemented but not tested, not yet ready for full use.
	
* v0.1:
    * Initial code release, not yet ready for full use.


## How do I get set up? ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

### Code Formatting ###

I'm using the ReSharper plugin for Visual Studio during code development and hence the code in this repository is formatted according to its guidelines.
If you don't have ReSharper, here are some basic guidelines you should follow if you intent to contribute to this repository:

* Use implicitly typed variables, e.g. use the `var` keyword for variable declaration whenever possible.
* Use CamelCase for Classes, Methods, Properties etc.
* Use camelCase for local variables.


Furthermore (non ReSharper specific):

* Use _camelCase for fields.
* Use descriptive, readable variable names. We don't live in the 80's when screen real-estate was precious. Example: `Win32HookExtensions` in stead of `W32HExt`.
	* An exception to this rule is when the datatypes are copied from the actual Windows API, such as structs as KBDLLHOOKSTRUCT or enums such as WM. The general rule of thumb: If something is copied 1-on-1 from the Windows API, use the same names. If not, use decent, human-readable .NET names.


### Comitting to the Repository ###

If you commit anything to the repository, please do so on the Develop branch and create a pull request to the master branch. Once your contributions have been reviewed I will merge them with the master branche if they meet the basic quality guidelines.

## Contact details ##

### Repository Owner ###
Maarten Thomassen

E-Mail: m.j.l.h.thomassen@gmail.com

Website: [withMartin]

[withMartin]: http://www.withmartin.net
[MSDN Hooks]: http://msdn.microsoft.com/en-us/library/windows/desktop/ms632589%28v=vs.85%29.aspx